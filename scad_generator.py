from jinja2 import Environment, FileSystemLoader
import subprocess
import os

def create_folder_if_not_exists(folder_path):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
        print(f"Folder '{folder_path}' created.")
    else:
        print(f"Folder '{folder_path}' already exists.")


def compile_openscad(openscad_file, output_file):
    """
    Compile OpenSCAD file to produce the output file.
    """

    command = ['openscad', '-o', output_file, openscad_file]

    # Run the command
    subprocess.run(command)


template_dir = './templates'
env = Environment(loader=FileSystemLoader(template_dir))
template = env.get_template('layout.scad.j2')

coordinates_list = [
    [25, 50, 24],
    [55, 50, 24],
    [82, 67, 24],
    [94, 114, 30],

    [109, 21, 24],
    [133, 6, 24],
    [164, 5, 24],

    [109, 52, 24],
    [133, 37, 24],
    [164, 38, 24],
    [196, 47, 24],

    [109, 83, 24],
    [133, 68, 24],
    [164, 69, 24],
    [196, 78, 24],
]

left_margin = 12
top_margin = 12
new_coordinates = list()
for coordinates in coordinates_list:
    new_coordinates.append([coordinates[0]+left_margin, coordinates[1]+top_margin, coordinates[2]])

rendered_code = template.render(
    coordinates_list=new_coordinates,
)

with open("libraries/generated_layout.scad", "w") as f:
    f.write(rendered_code)

if __name__ == "__main__":
    create_folder_if_not_exists("models")
    compile_openscad("generated_lid.scad", "models/lid.stl")
    compile_openscad("generated_top.scad", "models/top.stl")