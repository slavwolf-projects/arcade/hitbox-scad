include <parameters.scad>

button_hole_depth = 8;
bottom_thickness = 1;
square_side = 13.8;

options_button_size = 7;

module keyswitch_hole(button_size) {
    difference() {
        keyswitch_avoid_spaghetti(button_size);
        keyswitch_square_mount();
    }
}

module keyswitch_square_mount() {
    translate([-square_side/2, -square_side/2, 0]) {
        cube([square_side, square_side, button_hole_depth], center = false);
    }
}

module keyswitch_avoid_spaghetti(button_size) {
    union() {
        translate([0,0,button_hole_depth-1]) {
            difference() {
                cylinder(h = bottom_thickness, d = button_size + 1, $fn = 200);
                translate([-square_side, -square_side/2, 0]) {
                    cube([button_size*4, square_side, 0.2]);
                }
            }
        }
        difference() {
            cylinder(h = button_hole_depth, d = button_size + 1);
            cylinder(h = button_hole_depth, d = button_size);
        }
    }
}

module options_button() {
    cylinder(h = panel_thickness, d = options_button_size, $fn = 200);
}

module option_buttons_array() {
    options_button();
    translate([15, 0, 0]) options_button();
    translate([30, 0, 0]) options_button();
    translate([45, 0, 0]) options_button();
    translate([60, 0, 0]) options_button();
    translate([75.0, 0, 0]) options_button();
}