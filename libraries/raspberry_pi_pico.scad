pico_hole_diameter = 2;
pico_short_distance = 11.4;
pico_long_distance = 48.26;

module raspberry_pico_mount() {
    translate([0, pico_long_distance, 0]) {
        raspberry_pico_mount_post();
    }
    translate([pico_short_distance, pico_long_distance, 0]) {
        raspberry_pico_mount_post();
    }
    translate([0, 0, 0]) {
        raspberry_pico_mount_post();
    }
    translate([pico_short_distance, 0, 0]) {
        raspberry_pico_mount_post();
    }
}

module raspberry_pico_mount_post() {
    difference() {
		cylinder(r=pico_hole_diameter*2, h=3, $fn=50, center=false);
		cylinder(r=pico_hole_diameter/2, h=3, $fn=50, center=false);
	}
}
