use <raspberry_pi_pico.scad>

module hitbox_top() {
    wall_translate = 2*wall_thickness;
    union() {
        difference() {
            roundedBox(length, width, height, cornerRadius); 
            translate([wall_thickness,wall_thickness,panel_thickness]) {
                roundedBox(length-wall_translate, width-wall_translate, height-panel_thickness, cornerRadius); 
            }
            panel_button_holes();
            translate([18,8,0]) option_buttons_array();
            translate([110,0,4]) cable_hole();
        }
    panel_keyswitch_holes();
    lid_mount();
    translate([30, 110, panel_thickness]) raspberry_pico_mount();
    }
}

module roundedBox(length, width, height, radius)
{
    dRadius = 2*radius;
    minkowski() {
        cube(size=[length-dRadius, width-dRadius, height]);
        cylinder(r=radius, h=0.01);
    }
}

module lid_mount() {
	translate([length-20-wall_thickness, width-20-wall_thickness, panel_thickness]) m3_mount_post();
	translate([length-20-wall_thickness, wall_thickness, panel_thickness]) m3_mount_post();
	translate([wall_thickness, width-20-wall_thickness, panel_thickness]) m3_mount_post();
	translate([wall_thickness, wall_thickness, panel_thickness]) m3_mount_post();
}

module m3_mount_post() {
	difference() {
		cylinder(r=m3_screw_selftap_radius*4, h=height-panel_thickness, $fn=6, center=false);
		cylinder(r=m3_screw_selftap_radius, h=height-panel_thickness, $fn=50, center=false);
	}
}

module cable_hole() {
    rotate([90,0,0]) cube([12,6,10]);
}