include <buttons.scad>

button_24mm = 25;

module panel_button_holes() {
    translate([62, 193, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([62, 163, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([79, 136, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([123, 124, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([33, 109, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([18, 85, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([17, 54, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([64, 109, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([49, 85, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([50, 54, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([59, 22, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([95, 109, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([80, 85, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([81, 54, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }

    translate([90, 22, 0]) {
        cylinder(h = button_hole_depth, d = button_24mm);
    }
}

module panel_keyswitch_holes() {
    translate([62, 193, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([62, 163, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([79, 136, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([123, 124, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([33, 109, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([18, 85, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([17, 54, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([64, 109, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([49, 85, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([50, 54, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([59, 22, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([95, 109, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([80, 85, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([81, 54, 0]) {
        keyswitch_hole(button_24mm);
    }

    translate([90, 22, 0]) {
        keyswitch_hole(button_24mm);
    }
}