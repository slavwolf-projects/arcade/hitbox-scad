include <buttons.scad>

module panel_button_holes() {

    translate([37, 62, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([67, 62, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([94, 79, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([106, 126, 0]) {
        cylinder(h = button_hole_depth, d = 31 + 1);
    }

    translate([121, 33, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([145, 18, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([176, 17, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([121, 64, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([145, 49, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([176, 50, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([208, 59, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([121, 95, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([145, 80, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([176, 81, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

    translate([208, 90, 0]) {
        cylinder(h = button_hole_depth, d = 25 + 1);
    }

}

module panel_keyswitch_holes() {

    translate([37, 62, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([67, 62, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([94, 79, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([106, 126, 0]) {
        keyswitch_hole(30 + 1);
    }

    translate([121, 33, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([145, 18, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([176, 17, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([121, 64, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([145, 49, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([176, 50, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([208, 59, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([121, 95, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([145, 80, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([176, 81, 0]) {
        keyswitch_hole(24 + 1);
    }

    translate([208, 90, 0]) {
        keyswitch_hole(24 + 1);
    }

}