dWallThickness = 2*wall_thickness;

module hitbox_lid() {
    difference() {
        roundedBox(length, width, 1, cornerRadius);
        lid_screw_holes();

    }
    difference() {
        translate([wall_thickness,wall_thickness,0]) {
            roundedBox(length-dWallThickness,width-dWallThickness,4,cornerRadius);
        }
        translate([wall_thickness+1,wall_thickness+1,0]) {
            roundedBox(length-dWallThickness-2,width-dWallThickness-2,4,cornerRadius);
        }
    }
}

module lid_screw_holes() {
    translate([length-20-wall_thickness, width-20-wall_thickness, 0]) {
        cylinder(r=m3_screw_selftap_radius, h=panel_thickness, $fn=50, center=false);
    }
    translate([length-20-wall_thickness, wall_thickness, 0]) {
        cylinder(r=m3_screw_selftap_radius, h=panel_thickness, $fn=50, center=false);
    }
    translate([wall_thickness, width-20-wall_thickness, 0]) {
        cylinder(r=m3_screw_selftap_radius, h=panel_thickness, $fn=50, center=false);
    }
    translate([wall_thickness, wall_thickness, 0]) {
        cylinder(r=m3_screw_selftap_radius, h=panel_thickness, $fn=50, center=false);
    }
}
